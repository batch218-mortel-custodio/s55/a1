//import coursesData from '../data/coursesData.js';
import CourseCard from '../components/CourseCard.js';

import { useState, useEffect } from 'react';

export default function Courses(){

	const [courses, setCourses]  = useState([]);


	//console.log(coursesData);
	// so that you dont need to write <CourseCard> multiple times here...
	// const courses = coursesData.map(course => {
	// 	return(
	// 		<CourseCard key={course.id} course={course}/>
	// 	)
	// })


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/active`)
		.then(res => res.json())
		.then(data => {
		    console.log(data);

		    setCourses(data.map(course => {
		    	return <CourseCard key={course._id} course={course}/>
		    }))
		})
	}, []);

	// then render the variable here that contains the CourseCard contents, regardless if thousands of courses Exist
	return(
		<>
		{courses}
		</>
	)
}