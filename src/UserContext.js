import React from 'react';


// react.createcontext --> allows us to pass information between components without using props drilling


const UserContext = React.createContext();


// this allows other components to consume/use the context object and supply ng necessary information to the context object
export const UserProvider = UserContext.Provider;

export default UserContext;

